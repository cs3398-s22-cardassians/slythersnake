# Slyther Snake
> ### Description: Our software engineering team: Leo, Jayla, Steve, Shizene, Lauren. 
> _SlytherSnake_ is a fresh new take on the classic game _Snake_. The player controls a snake from a top-down perspective and must navigate a three-dimensional environment while dodging enemies and obstacles. The game is developed in Unity, and we decided to take this project as a team in order to learn more about Unity, C#, and software engineering principles.
> Live demo [_here_](https://www.example.com). <!-- If you have the project hosted somewhere, include the link here. -->


## General Information
> ![SnakeIcon](https://play-lh.googleusercontent.com/g6IsfSZlxGLinzisCUqp3MaxqkoKbgwRch-9u_bryQS-XYiMlDb5uHCxQ-LCDgpNv4Y)
<!-- You don't have to answer all the questions - just the ones relevant to your project. -->

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Features](#features)
* [Screenshots](#screenshots)
* [Setup](#setup)
* [Usage](#usage)
* [Project Status](#project-status)
* [Room for Improvement](#room-for-improvement)
* [Acknowledgements](#acknowledgements)
* [Contact](#contact)
<!-- * [License](#license) -->



## Technologies Used
- Tech 1 - BitBucket
- Tech 2 - GitKraken
- Tech 3 - Unity
- Tech 4 - CircleCi
- Tech 5 - Bootstrap5


## Features
List the ready features here:
### Dedicated Website: 
> - The game will be shared on its own website. Using Bootstrap5 as the framework, it will include information about the game, not limited to: how to play the game, the rules, and information on the power ups. Related stories are exporting the game in unity and creating the website.
### Revamped Snake: 
> - SlytherSnake will be a new and imporved take on the original snake game. With three dimensional elements, customizability, enemies to dodge, and powerups, it will certainly offer new gameplay. Related stories are learning how to code in unity and learning how to create game environements in Unity.   
### Different Playable Snake Characters: 
> - With at least two different options of different SlytherSnakes, each snake will look and play slightly different. Related stories are costumes and characters. 


## Screenshots
![SnakeIcon]()
<!-- If you have screenshots you'd like to share, include them here. -->


## Setup
What are the project requirements/dependencies? Where are they listed? A requirements.txt or a Pipfile.lock file perhaps? Where is it located?

Proceed to describe how to install / setup one's local environment / get started with the project.


## Usage
How does one go about using it?
Provide various use cases and code examples here.

`write-your-code-here`


## Project Status
### Project is: _in progress_
>- We are currently at the end of Sprint 1 and will demo our initial build. 

## Room for Improvement
Include areas you believe need improvement / could be improved. Also add TODOs for future development.

Room for improvement:
> - Improvement to be done 1
> - Improvement to be done 2

To do:
> - Feature to be added 1
> - Feature to be added 2
___

## Sprint 1

### Individual Contributions
#### Leonardo:
> - Familiarize with Unity and C# [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/b7afac5f10fbabb07ccbbe7f0982cf4e23edc2ec)
> - Design basic game environment. This will be our starter environment in which we will demo any contributions. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/f6c21eb1d308cad73d5ca952f7333440186ffaec) [commit2](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/30697adec162120eaacc725e194e37dd1413dae8).
> - Study HTML and Bootsrap, this will be template website we use. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/0e0a494371286c49afed6fd86dca8b76884dca07)
> - Study React and implement basic React features. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/324edee9b3f1b6b022b6125b3b322c4e9620bf8a)
> - Modify Starter Bootstrap Website. Modified website to better suit hosting the game. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/cd4ed97c17e5a974fa02b7f1faf3a438f6e55f2d)

#### Shizene:
> - Familiarize with unity [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/a467994a3cdcf008c8932ca6834a3432c89b11f3)
> - Learn javascript and jquery[commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/f5f0d9f0a6b2b7ff8c5fe73c371ff89ecab3160a)
> - Brush on HTML and CSS [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/dfa85deec062e56d7d907bc8353065d8b49da102)
> - Learn Boostrap to implement features on the website [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/bd6c21921e530bbfde0b5c44d278c9436d7fed62)
> - Learn to create basic [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/f5778951f0d1c662a11fd18983b092ff1de2fbe0) 

#### Lauren: 
> - SNAK-14 Brush up on Javascript and JQuery. <a href="https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/68626edd784cf4259eb7172ce808892fe94bda4f">(Commit)</a> 
> - SNAK-18 Brush up on HTML and Bootstrap. <a href="https://bitbucket.org/cs3398-s22-cardassians/slythersnake/src/SnakeGame-docs-llt31/SNAK-14-brush-up-on-javascript-and-jquery">(Commit)</a> 
> - SNAK-22 Get familiar with basic concepts of Unity and C#. <a href="https://bitbucket.org/cs3398-s22-cardassians/slythersnake/src/SnakeGame-docs-llt31/SNAK-22-get-familiar-with-basic-concepts.md">(Commit)</a>
> - SNAK-25 Familiarize with React and its basic features. <a href="https://bitbucket.org/cs3398-s22-cardassians/slythersnake/src/SnakeGame-docs-llt31/SNAK-25-familiarize-with-react">(Commit)</a>
> - SNAK-28 Help Design Basic Character Options. <a href="https://bitbucket.org/cs3398-s22-cardassians/slythersnake/src/leonardo/SNAK-28-help-design-basic-character-options">(Commit)</a>

#### Steve:
> - Basic Unity tutorial learning which will help me build the power ups for the game [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/c90a3e6bbdb65955241005f36b10ede8d99c2f46)
> - Basic HTML and CSS learning which will help me build the website for the game [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/27b3236b5cb3f9bd1fe6bfcfcfbfefcefa1471b3)
> - Boostrap and its features learning which will help me learn another way to build a website for the game [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/c63cc334d0e413be935f93e6a54e2826ed7aa945)
> - Snake game feature implementations. Ex: speedup, color change which are the actual power up in the game [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/5b7c4fd9886cceb523021e8060d133fbbfd14b77)
 
#### Jayla:
> - Created basic scene as a template with a ground and player object. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/eacc736a0a3495d035db3b6c7e2e6669c67f97e3)
> - Created scripts for snake function grow and move. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/f6b4e49cf10899107bbd32d4d62a7d96eaf0d230)
> - Created and configured player prefab. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/2f6dae991acd4777bc780e54417fe34da7e35731)
> -  Created Snake Head shape in blender [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/d6ccf44734cd304cffde2c5ad3e4fe6699ebf616)
___

## Sprint 1 Retrospective

### What went well?

#### Team:
- The communication of each task from every team member was very fluid. 
- Time management was affective, we always found time to meet as a team outside of class.
- Everyone was very proactive with their tasks and helped each other with the resources.
- Everyone in the group got along well with each other. 
- We all have great attitudes when it comes to challenges.
#### Individual:
##### Steve:
-  I was able to manage and create my first unity power up with the help of my team mate Jayla House, and was able to familiarize myself with C Sharp when using Unity. 
##### Jayla:
- Using someone else's code and being able to edit it and use it for my game without too much trouble.
##### Shizene:
- I was able to get myself familiarize with unity. 
##### Lauren:
- learning unity and learning how to make character skins or changes.
##### Leonardo:
- Making a game environment for the snake to move around in. 
### What can I do to improve? 
##### Steve:
- I could be more organise with my work so that I don't scramble all the work to be done on due date or the last two days of the due date.
##### Shizene:
- I could get more familiar with unity. Also learn blender so I can help Jayla with the objects for the game. 
##### Leonardo: 
- Having a clear goal in my mind and visualizing it. I spent too much time thinking instead of simply working. 
##### Jayla:
- I could get more familiar with blender so that when transporting from blender to unity things work how they are intended. 
##### Lauren:
- be more involved on the coding side of things in the Snake Game, my first sprint was me just getting familiar with everything. As well as getting more familiar with unity.

### What might be impending us from performing better?
- Our other classes that require a lot of time and energy.
- Our inexperience with technologies such as Bitbucket and Gitkracken

___

## SPRINT 2: Next Steps


### Leonardo:
> - Create at least two more sophisticated levels that will be utilized in the actual game. Consider using video game level design principles.
> - Fully migrate a version of the game to be playable on the website
> - Implement a feature of completing level one, then being sent to level2

### Shizene: 
> - Create more realistic looking skin. currently we have a very basic version of snake would like to add skins to the snake to make it look realisitc.
> - Add a feature to add snake lives, it can only hold up to 3 lifes.
> - Add a kill feature for the snake. If the snake hits certain walls of the game it dies if there are no lives.
> - Create more attractive powerups which a player can distinguish from one another. 

### Lauren:
> - Customize snake and create character options for snake skins
> - Assist with building the website for the snake game using BootStrap
> - Integrate unit testing

### Steve:
> - Explore more power ups that can be implemented in the game.
> - Improve the color change power up so that it can change the snake whole body.
> - Implement a stop watch on the speed power up.

### Jayla:
> - Making the snake character and rigging it with a proper skeleton.
> - Create Variation in the snake envonment so that there are hazards the player needs to avoid. 
> - Creating a StartScreen


___

## Sprint 2

### Individual Contributions
#### Leonardo:
> - SNAK-44 Create full LevelOne. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/4cd3902016aa091c09d14dd501069908ef272f3a)
> - SNAK-45 Create full LevelTwo [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/6306ed075d2d9791698178934041c962f9f3d3c3)
> - SNAK-52 Add music, custom textures, and improved level design [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/374eafa11a7988cdc5471ba7861c5b6b57684e60)
> - SNAK-49 Connect levels via in-game progession [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/633b16683f0d4e8542688c0e9da9b7d5b4c18ed6)

#### Lauren:
> - SNAK-62 Create unit tests for PlayMode for SnakeController [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/af46f3f2f4a16bad8a2966d854a48822fdf78911)
> - SNAK-63 Create unit tests for EditMode [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/e79c04d1a5c8d5eca903f3149fa214df6d99412b)
> - SNAK-64 Add vegetation to level 2 [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/b7933a548f7903370ead790d1f9e7c14063f9225)

#### Jayla:
> - SNAK-47 create pause screen and tutorial screen [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/bd2ab8a64b94dacf7c5bccb6b9e79d75b2fffe4a)
> - SNAK-48 implement all the UI into the game [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/1d047e7ccdf0fa84b8067061fa96ac7067c5428c) 
> - SNAK-46 Create start screen and end game screen [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/bd2ab8a64b94dacf7c5bccb6b9e79d75b2fffe4a)
> - GameUI implement start screens so that they transfer betwwen levels [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/ab9a95ff28824981255f673a85d8e0d80d068036)

### Steve:
> - SNAK-56 Implement a timer on the snake when the speedup is activated [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/6a21ac7e67210bc9127ce51ef0d7d65236f86019)
> - SNAK-59 Change the snake whole body color after picking activating color change [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/7fe036fdc6cb0d35bd78b8046f30c931ccfa8da3)
> - SNAK-61 Add a timer on the screen to be visible to the user [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/11b0d6e6db9e8df995e27cccafa051709069b1f1)

### Shizene: 
> - SNAK-57 Adding Respwaning Script [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/fe1561d40765b3687b0ebe3088739ec7ccc1c7ef)
> - SNAK-58 Create level 3 [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/06d480637461e5446ec05dc11c7e3fd128e2b852)
> - SNAK-60 learn to use blender to create more appealing powerups [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/06db09f016196d8c0e4bf0415791e9015db7837c)

___

## Sprint 2 Retrospective

### What went well?

#### Team:
- We all did great with using bit bucket to work on our own features as well as merge our work together effectively. 
- We helped each other solve problems.
- We were able to get all our tasks implemented, merge, and done.
-  
- 
#### Individual:
##### Steve:
- What went well is me completing my given task without any blocks or help from anybody.  
##### Jayla:
- What went well was being able to merge my features with everyone elses and not have any compiler issues or clashes
##### Shizene:
- What went well was being able to create level 3.
##### Lauren:
- Customization of level 2 was fun and a rather great learning experience.
##### Leonardo:
- Merging the working branches together went well. Our respective work complimented each other.
### What can I do to improve? 
##### Steve:
- What I can do to improve is be more creative with the game power ups and be more knowledgeable on how to use Gitkraken.
##### Shizene:
- What I can do is improve my respwaing code so we can apply it for all our levels. 
##### Leonardo: 
- I need to explore more efficient options of how to get tasks done.
##### Jayla:
- I can be more effective with my time management when it comes to getting my task done in a timely manner. 
##### Lauren:
- I need to focus more thoroughly on my tasks as well as work on my time management skills.

### What might be impending us from performing better?
- We haven't been able to meet as a group this sprint to check base with each other. 
- Some of us still have a little bit of an issue with GitKracken, but it is getting much better.
- what might be impeding us are exams/midterms that are going on in our other classes which take our focus away from the project.
- Proper communication of responsibilities might be impending us a bit.
___

## SPRINT 3: Next Steps


### Leonardo:
> - Create two more additional levels to create more diversified gameplay.
> - Program a way for the player to lose.
> - Implemenent gameplay mechanics that the player can interact with in the levels.

### Lauren:
> - Add more unit testing for preexisting and new code such as the levels
> - Add customization to levels 3 and 4
> - Assist in making the snake character look more realistic using snake head and skins

### Jayla:
> - Editting/ polishing the snake movements and body parts to work more smoothly with player input.
> - Edditing the scripts to model SOLID principles.
> - Implement and creater a better looking powerups and character.

### Steve:
> - Display on the screen the amount of pick ups the snake completed in the game
> - Increase the game time after certain amount of pick ups
> - Add some image or logos on the screen to represent time or pick up to make the game look more interesting.

### Shizene:
> - Work on the reswaping script.
> - Use blender to create more appealing powerup
> - Implement difficulties in level 3

___

## Sprint 3

### Individual Contributions
#### Leonardo:
> - [SNAK-72](https://cs3398s22cardassians.atlassian.net/jira/software/projects/SNAK/boards/2?assignee=60ec5fd2081fcd0070a45148&selectedIssue=SNAK-72) Create full Level Four. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/4a6ebe99af6c570b7cc1750113d5418e6d67796e)
> - [SNAK-73](https://cs3398s22cardassians.atlassian.net/jira/software/projects/SNAK/boards/2?assignee=60ec5fd2081fcd0070a45148&selectedIssue=SNAK-73) Create full Level Five. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/4bfddbcc5d6ad71a33404f4d5d5549271af39e02)
> - [SNAK-74](https://cs3398s22cardassians.atlassian.net/jira/software/projects/SNAK/boards/2?assignee=60ec5fd2081fcd0070a45148&selectedIssue=SNAK-74) Decorate Level Four. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/85db8d9d20dbc2cbd4b05a226f4e00a06c860457)
> - [SNAK-76](https://cs3398s22cardassians.atlassian.net/jira/software/projects/SNAK/boards/2?assignee=60ec5fd2081fcd0070a45148&selectedIssue=SNAK-76) Refactor Level One and Two, Polish. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/0c9ac98b1ee77c38fbdde4d020956cd006eefd3e)
> - [SNAK-75](https://cs3398s22cardassians.atlassian.net/jira/software/projects/SNAK/boards/2?assignee=60ec5fd2081fcd0070a45148&selectedIssue=SNAK-75) Sprint2 Demo on Website. [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/3681e80474ff49153798356febc863ea7ae83e7c)

#### Jayla:
> - SNAK-77 Polish look and feel of snake [commit](https://bitbucket.org/%7B%7D/%7Bc2badc94-48f6-43fe-8fca-d6393cd05abb%7D/commits/5f585e285b3c60d558cc29cb2791f737533643ba)
> - SNAK-78 Add/dialog [commit](https://bitbucket.org/cs3398-s22-cardassians/%7Bc2badc94-48f6-43fe-8fca-d6393cd05abb%7D/commits/29be30f0504176e0ba3ce0080086c120c0091b8f) 
> - SNAK-91 Implement tutorial prompts [commit](https://bitbucket.org/%7B%7D/%7Bc2badc94-48f6-43fe-8fca-d6393cd05abb%7D/pull-requests/16)

#### Steve:
> - SNAK-85 Pick ups counter display [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/704b74035233479929776b7cefc15baa3496d55a)
> - SNAK-86 Increase game time power up (2 commits made) [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/d1e27496b7c8cf4c8049de5ebc37e027d57d1527), [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/ac20edaafe6c60f590860b7e35d41fa7f4e245ab)
> - SNAK-87 Image or Logo Added for representation of Timer(Stopwatch logo) and pick up counter(gold coin logo). An image of the task added in Jira, not much of the code was change [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/860c2de8aa5e1a8d4eb195c7c9f4a0eeb0e5867f)

#### Lauren:
> - SNAK-79 Create playmode tests for new and preexisting code [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/5fac9f1944316f7d6eadf8056c8d8b5003b491e4)
> - SNAK-80 Create editmode tests for new and preexisting code [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/fc2f9f891397eb05a784a4c6dcdbd902093e102a)
> - SNAK-83 Add vegetation and other decor to level 5 [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/ce3f1d49b52e71126d22b72992a32bb956044b70)
> - SNAK-84 Help add more character to the snake, snake skins [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/b09112ea60d10df7c4f187c1cd4e069b89639fd8)

#### Shizene:
> - SNAK-88 Polish Level 3[commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/ac39b5803b8dd43e70a83515dbad4cea4f06cb2e)
> - SNAK-89 Polish Reswaping code for player to respawn [commit](https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/ec84e59f67edb2b2302e1768003d9992bd1cf1c9)
> - SNAK-90 Work on difficulty for level 3 [commit] (https://bitbucket.org/cs3398-s22-cardassians/slythersnake/commits/451dfb61ddc4a5532b77243fe366646b153bef92)
___

## SPRINT 4: Next Steps

### Leonardo:
> - Fix collision issues and "noclip" areas that might be present in the levels.
> - Implement an auto-respawn function in the case that the player falls to outer bounds.
> - Look for other assets to import that will allow for more interesting level design.

### Jayla:
> - Polish the look of the power ups to not just be circles
> - Create a story for the game so it gives the player a mission
> - Create a enemy for the snake scharacter to display 

### Lauren:
> - Create more snake skins so the player can choose which skin/character to play in
> - Create more unit test as new code is being or written or built
> - Help with making the level transition noticable by adding more features and graphics to the game

### Steve:
> - Add a store in game where with the coins collected from the game the player can go to that store and purchase fun items in that store like new snake skins.
> - Add a power up which will give the snake the another chance to continue the game after being dead instead of restarting the level all over.
> - Add some new songs for each level in the game so theat each level will have it own music.

### Shizene:
> - Will do some unity testing for our project 
> - Going to implement multiplayer aspect to the game where 2 players can compete with each other.
> - Will work on implementing leaderboards for the players to track their progress. 
___

## Sprint 3 Retrospective

### What went well?

#### Team:
- Leonardo: Having a team member review our pull request and approve our merge went down well during this sprint.
- Jayla: We were able to merge all of our progress to make an effective Demo(final product)
- Steve: We were able to overcome some of our merge conflicts we had while merging our branches in the demo branch. 
- test
- Lauren: We became better with using GitKraken and had great communication between us about tasks and conflicts. 
- Shizene: We were able to present a great working demo as everyone in the class did their task in the timely manner and were able to persent a great working demo for the standup. 

### Individual:

##### Leonardo: 
- I was able to get my work done in the appropriate amount of time. I did not overestimate nor underestimate the amount of time a task would take.

##### Jayla: 
- I was able to overcome the hard task of recrating the player character with better movements and making it able to work in our current game with correct behaviors 

##### Steve:
- I was able overcome one of my blocks through out this sprint thankfully with the help of my teammates

##### Lauren:
- I learned a lot about unit testing with this sprint, and my teammates were able to help me if I ran into a block.

##### Shizene: 
- I was able to learn a lot about unity and blender. All my teammates were very helpful in all the sprints. 

### What might be impending us from performing better?

##### Leonardo:
- I feel like communication was a little lacking this Sprint. Though this is a little expected due to the end of the semester arriving.

##### Jayla:
- This last sorint it seemed like veryone got caught up in their own work and we didint collaberate on task very often 

##### Steve:
- I think is our other classes where because were going through our last weeks of classes where a lot are assignments or projects are due which lead to a decrease in communication in the team about this sprint.

##### Lauren: 
- Although we had great communication via Slack, we did not meet up face to face in this sprint which could of helped us complete our tasks in a timely manner.

##### Shizene: 
- Time constraints during the last sprint caused communication issues. Would had been better if we had meet up in person.

### What can I do to improve? 

##### Leonardo: 
- Putting in more consistent time with my tasks. My team would be able to see the impacts of this improvement by noticing that my task submissions are more evenly spread out instead of done in sudden bursts.

##### Jayla:
- I could have gotten my work done at a more timely manner. 

##### Steve:
- I could be more creative with my tasks or come up with more power ups for the game. This would make the game more fun and enjoyable with everything my teammates added to the game and will take our snake game 
  above other snake games and will not be compare to other regular snake games.

##### Lauren:
-   Focus on my tasks more and watch my time management (say no to work so I can work on homework), so I could have added more tests. As well, help my teammates out with more levels. 

##### Shizene: 
- I could had managed my time better so I could be add more creativity to the project.  

## Acknowledgements
Give credit here.
- This project was inspired by...
- This project was based on [this tutorial](https://www.example.com).
- Many thanks to...


## Contact
Creaters: Leo, Jayla , Steve , Shizene , Lauren 


<!-- Optional -->
<!-- ## License -->
<!-- This project is open source and available under the [... License](). -->

<!-- You don't have to include all sections - just the one's relevant to your project -->